package com.apple.exercise.applet;

import javacard.framework.*;

/**
 * This class is a dummy applet that test the HMAC and CMAC implementation
 * 
 * @author Franck Rakotomalala
 *
 */
public class DummyApplet extends Applet {

    // specific CLA expected by this applet
    private final static byte CLA_SPECIFIC = (byte) 0xB0;

    // specific INS expected by this applet
    private final static byte INS_SPECIFIC = (byte) 0x00;

    // Length of SHA1 signature
    private final static short SHA1_SIGNATURE_LENGTH = 20;

    // Length of CMAC
    private final static short DES_CMAC_LENGTH = 8;

    // P1 value when HMAC provided
    private final static byte P1_WHEN_DATA_WITH_HMAC = (byte) 0x01;

    // P1 value when CMAC is provided
    private final static byte P1_WHEN_DATA_WITH_CMAC = (byte) 0x02;

    // assuming a fixed HMAC Key
    private final static byte[] HMAC_KEY = new byte[] { (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };

    // assuming a fixed CMAC Key (DES CBC)
    private final static byte[] CMAC_KEY = new byte[] { (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };

    public static void install (byte bArray[], short bOffset, byte bLength)
        throws ISOException {
        new DummyApplet ().register ();
    }

    public void process (APDU apdu) throws ISOException {

        if (selectingApplet ()) {
            // nothing to do
            return;
        }

        byte[] buf = apdu.getBuffer ();

        if (buf[ISO7816.OFFSET_CLA] != CLA_SPECIFIC) {
            ISOException.throwIt (ISO7816.SW_CLA_NOT_SUPPORTED);
        }
        if (buf[ISO7816.OFFSET_INS] != INS_SPECIFIC) {
            ISOException.throwIt (ISO7816.SW_INS_NOT_SUPPORTED);
        }

        switch ( buf[ISO7816.OFFSET_P1] ){

            case P1_WHEN_DATA_WITH_HMAC : // HMAC is provided
                hmac (apdu);
                return;

            case P1_WHEN_DATA_WITH_CMAC : // CMAC is provided
                cmac (apdu);
                return;

            default:
                ISOException.throwIt (ISO7816.SW_WRONG_P1P2);
        }
    }

    private void hmac (APDU apdu) {

        byte[] buf = apdu.getBuffer ();

        short lc = (short) ( buf[ISO7816.OFFSET_LC] & 0xFF );
        short read = apdu.setIncomingAndReceive ();

        while ( read < lc ) {
            read += apdu.receiveBytes (read);
        }

        /*
         * calculates offset where the data itself ends, assuming that the last
         * 20 bytes of the CDATA is the HMAC
         */
        short offsetEndData = (short) ( ( lc - SHA1_SIGNATURE_LENGTH ) & 0xFF );

        // get data from APDU ==> CDATA[0] to CDATA[offsetEndData-1]
        byte[] data = new byte[offsetEndData];
        Util.arrayCopy (buf, ISO7816.OFFSET_CDATA, data, (short) 0,
            offsetEndData);

        // get HMAC from APDU
        byte[] hmac = new byte[SHA1_SIGNATURE_LENGTH];
        Util.arrayCopy (buf, (short) ( ISO7816.OFFSET_CDATA + offsetEndData ),
            hmac, (short) 0, SHA1_SIGNATURE_LENGTH);

        /*
         * compute the expected HMAC according to data value (DOESN'T
         * INCLUDE THE 20 LAST BYTES HMAC)
         */
        byte[] expectedHmac = HMAC.computeHmac (data, HMAC_KEY);

        // compare provided hmac with the computed one
        byte isEqual = Util.arrayCompare (expectedHmac, (short) 0, hmac,
            (short) 0, SHA1_SIGNATURE_LENGTH);

        // throws Wrong data SW in case the hmac doesn't match
        if (isEqual != (byte) 0x00) {
            ISOException.throwIt (ISO7816.SW_WRONG_DATA);
        }
    }

    private void cmac (APDU apdu) {
        byte[] buf = apdu.getBuffer ();

        short lc = (short) ( buf[ISO7816.OFFSET_LC] & 0xFF );
        short read = apdu.setIncomingAndReceive ();

        while ( read < lc ) {
            read += apdu.receiveBytes (read);
        }

        /*
         * calculates offset where the data itself ends, assuming that the last
         * 8 bytes of the CDATA is the CMAC
         */
        short offsetEndData = (short) ( ( lc - DES_CMAC_LENGTH ) & 0xFF );

        // get data from APDU ==> CDATA[0] to CDATA[offsetEndData-1]
        byte[] data = new byte[offsetEndData];
        Util.arrayCopy (buf, ISO7816.OFFSET_CDATA, data, (short) 0,
            offsetEndData);

        // get CMAC from APDU
        byte[] cmac = new byte[DES_CMAC_LENGTH];
        Util.arrayCopy (buf, (short) ( ISO7816.OFFSET_CDATA + offsetEndData ),
            cmac, (short) 0, DES_CMAC_LENGTH);

        /*
         * compute the expected HMAC according to data value (DOESN'T
         * INCLUDE THE 8 LAST BYTES CMAC)
         */
        byte[] expectedCmac = CMAC.computeCmac (data, CMAC_KEY);

        // compare provided hmac with the computed one
        byte isEqual = Util.arrayCompare (expectedCmac, (short) 0, cmac,
            (short) 0, DES_CMAC_LENGTH);

        // throws Wrong data SW in case the hmac doesn't match
        if (isEqual != (byte) 0x00) {
            ISOException.throwIt (ISO7816.SW_WRONG_DATA);
        }

    }
}
