package com.apple.exercise.applet;

import javacard.framework.Util;
import javacard.security.DESKey;
import javacard.security.KeyBuilder;
import javacardx.crypto.Cipher;

/**
 * This class is the implementation of CMAC algorithm.
 * Here the C-MAC is based on DES CBC algorithm.
 * 
 * @author Franck Rakotomalala
 *
 */
public class CMAC {

    private final static byte[] IV = new byte[8];

    /**
     * This function computes the C-MAC of the given message by using the fixed DES-CBC 16 bytes
     * Key.
     */
    public static byte[] computeCmac (byte[] data, byte[] key) {

        // will contain final result
        byte[] result = new byte[8];

        // will contain each 8 bytes block of data
        byte[] dataBlock = new byte[8];

        // padding received data in case it's not multiple of 8
        data = pad (data);

        short counter = 0;
        while ( counter < data.length ) {

            // copy next 8 bytes into temporary array
            Util.arrayCopy (data, (short) counter, dataBlock, (short) 0,
                (short) 8);

            if (counter == 0) {
                // first XOR with IV
                dataBlock = xor (dataBlock, IV);
            } else {
                // XOR the previous encrypted block with the next block
                dataBlock = xor (dataBlock, result);
            }

            // encrypt with DES-CBC the current XORed block
            result = desCBC (dataBlock, key);

            // increase counter to 8 more
            counter = (short) ( counter + 8 );
        }

        return result;
    }

    /**
     * This function encrypt a data block of 8 bytes with DES-CBC algorithm.
     */
    public static byte[] desCBC (byte[] dataBlock, byte[] key) {

        byte[] finalResult = new byte[8];
        byte[] tempResult = new byte[16];

        DESKey deskey = (DESKey) KeyBuilder.buildKey (KeyBuilder.TYPE_DES,
            KeyBuilder.LENGTH_DES, false);
        deskey.setKey (key, (short) 0);

        Cipher c = Cipher.getInstance (Cipher.ALG_DES_CBC_ISO9797_M2, false);
        c.init (deskey, Cipher.MODE_ENCRYPT);

        c.doFinal (dataBlock, (short) 0, (short) dataBlock.length, tempResult,
            (short) 0);

        // copy the first 8 bytes of doFinal output.
        Util.arrayCopy (tempResult, (short) 0, finalResult, (short) 0,
            (short) 8);

        return finalResult;
    }

    /**
     * This function computes XoR operation between two byte arrays
     */
    private static byte[] xor (byte[] part1, byte[] part2) {

        part1 = pad (part1);
        part2 = pad (part2);

        byte[] result = new byte[8];
        for (int i = 0; i < 8; i++) {
            result[i] = (byte) ( part1[i] ^ part2[i] );
        }
        return result;
    }

    /**
     * This function adds padding on a given byte array according to ISO9797-4 specification.
     */
    private static byte[] pad (byte[] data) {
        byte[] result = null;
        short paddingByteCounter = 0;

        // count the needed padding length
        while ( ( ( data.length + paddingByteCounter ) % 8 ) != 0 ) {
            paddingByteCounter++;
        }

        if (paddingByteCounter > 0) {

            // first byte of padd shall be 80, the rest will be filled with 00
            byte[] padd = new byte[paddingByteCounter];
            padd[0] = (byte) ( 80 & 0xFF );

            // new byte array that will contain the padded data
            result = new byte[data.length + paddingByteCounter];

            Util.arrayCopy (data, (short) 0, result, (short) 0,
                (short) data.length);

            Util.arrayCopy (new byte[] { (byte) ( 128 & 0xFF ) }, (short) 0,
                result, (short) data.length, (short) 1);
        }
        if (result == null)
            return data;

        return result;
    }
}
